function validateform()
{
	var clinicName = document.forms["registrationForm"]["clinic"].value;
	var doctorName = document.forms["registrationForm"]["doctor"].value;
	var address1 = document.forms["registrationForm"]["address"].value;
	//var address2 = document.forms["registrationForm"]["address2"].value;
	var town = document.forms["registrationForm"]["town"].value;
	//var postCode = document.forms["registrationForm"]["postcode"].value;
	var email = document.forms["registrationForm"]["email"].value;
	//var tel = document.forms["registrationForm"]["telephone"].value;
	//var fax = document.forms["registrationForm"]["fax"].value;
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	var cert_reg = document.forms["registrationForm"]["cert_reg"];
	var cer1 = document.forms["registrationForm"]["cert1"].value;
	var cer2 = document.forms["registrationForm"]["cert2"].value;
	var reg1 = document.forms["registrationForm"]["reg1"].value;
	var reg2 = document.forms["registrationForm"]["reg2"].value;
	
	var success = 1;
	
	if(cert_reg[0].checked == true)
	{
		if(cer1 == null || cer1 == "")
		{
			alert("Filled in cert info");
			
			return false;
		}
		
		if(cer2 == null || cer2 == "")
		{
			alert("Filled in cert info");
			
			return false;
		}
	}
	
	if(cert_reg[1].checked == true)
	{
		if(reg1 == null || reg1 == "")
		{
			alert("Filled in mmc info");
			
			return false;
		}
		
		if(reg2 == null || reg2 == "")
		{
			alert("Filled in mmc info");
			
			return false;
		}
	}
	
	if(clinicName == null || clinicName == "")
	{
		alert('Filled in Clinic Name')
		
		return false;
	}
	
	
	if(doctorName == null || doctorName == "")
	{
		alert('Filled in Doctor Name')
		
		return false;
	}

	if(address1 == null || address1 == "")
	{
		alert('Filled in Address')
		
		return false;
	}
	
	if(town == null || town == "")
	{
		alert('Filled in town')
		
		return false;
	}

	if(atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		alert("Invalid email address");
		
		return false;
	}
	
	if(success == 1)
	{
		alert("Thank you for your registration !");
	}
	

}