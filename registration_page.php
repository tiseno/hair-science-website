<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link href="css/stylesheet.css" rel="stylesheet" type="text/css">
	<!--Sliding Box--->
    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.js"></script>
    <script type="text/javascript" src="js/sliding_box.js"></script>
    <!--Sliding Box End--->
	
	<script type="text/javascript" src="formvalidation.js"></script>
	
	<!-- Diable/Enable Radio Button Script -->
	<script type="text/javascript">
	function setvalue()
	{
		document.getElementById('annual_practice').checked = true
		
		if (document.getElementById('annual_practice').checked = true)
		{ 
			document.getElementById('id_cert1').disabled='';
			document.getElementById('id_cert2').disabled='';
			
			document.getElementById('id_reg1').disabled='disabled';
			document.getElementById('id_reg2').disabled='disabled';
		}
	}
		window.onload = setvalue;
	
	function disablefield()
	{ 
		if (document.getElementById('annual_practice').checked == 1)
		{ 
			document.getElementById('id_cert1').disabled='';
			document.getElementById('id_cert2').disabled='';
			
			document.getElementById('id_reg1').disabled='disabled';
			document.getElementById('id_reg2').disabled='disabled';
		}
		
		if(document.getElementById('mmc_registration').checked == 1)
		{
			document.getElementById('id_reg1').disabled='';
			document.getElementById('id_reg2').disabled='';

			document.getElementById('id_cert1').disabled='disabled';
			document.getElementById('id_cert2').disabled='disabled';
		}
	}
	
	function resetFunction()
	{
			document.getElementById('annual_practice').checked = true;
			
			document.getElementById('id_cert1').disabled='';
			document.getElementById('id_cert2').disabled='';
			
			document.getElementById('id_reg1').disabled='disabled';
			document.getElementById('id_reg2').disabled='disabled';
			
			document.getElementById('id_cert1').value=null;
			document.getElementById('id_cert2').value=null;
			document.getElementById('id_reg1').value=null;
			document.getElementById('id_reg2').value=null;
			
			document.getElementById('id_clinic').value=null;
			document.getElementById('id_doctor').value=null;
			document.getElementById('id_address1').value=null;
			document.getElementById('id_address2').value=null;
			document.getElementById('id_town').value=null;
			document.getElementById('id_postcode').value=null;
			document.getElementById('id_email').value=null;
			document.getElementById('id_fax').value=null;
			document.getElementById('id_telephone').value=null;
	}
	
	</script>
	
	<title>Welcome to Hair Sciences - Understanding Hair Loss</title>
</head>

<?php
	if(isset($_POST['btn_submit']))
	{
		include "functions.php";

		//Keep some of the value after submit / alert
		//$cert = $_POST['cert']; 	//$reg = $_POST['reg'];
		
		$cert_reg = $_POST['cert_reg'];
		
		if($cert_reg != "")
		{
			if($cert_reg == "Annual Practicing Certificate No")
			{
				$cert = $cert_reg;
				$reg = "";
			}
			else
			{
				$reg = $cert_reg;
				$cert = "";
			}
		}
		
		if(isset($_POST['cert1'])){$cert1 = $_POST['cert1'];}else{$cert1 = "";}
		if(isset($_POST['cert2'])){$cert2 = $_POST['cert2'];}else{$cert2 = "";}
		if(isset($_POST['reg1'])){$reg1 = $_POST['reg1'];}else{$reg1 = "";}
		if(isset($_POST['reg2'])){$reg2 = $_POST['reg2'];}else{$reg2 = "";}
		if(isset($_POST['clinic'])){$clinic = $_POST['clinic'];}else{$clinic = "";}
		if(isset($_POST['doctor'])){$doctor = $_POST['doctor'];}else{$doctor = "";}
		if(isset($_POST['address'])){$address = $_POST['address'];}else{$address = "";}
		if(isset($_POST['address2'])){$address2 = $_POST['address2'];}else{$address2 = "";}
		if(isset($_POST['town'])){$town = $_POST['town'];}else{$town = "";}
		if(isset($_POST['postcode'])){$postcode = $_POST['postcode'];}else{$postcode = "";}
		if(isset($_POST['email'])){$email = $_POST['email'];}else{$email = "";}
		if(isset($_POST['telephone'])){$telephone = $_POST['telephone'];}else{$telephone = "";}
		if(isset($_POST['email'])){$email = $_POST['email'];}else{$email = "";}
		if(isset($_POST['fax'])){$fax = $_POST['fax'];}else{$fax = "";}

		$date = date('d/m/y', time());

		//Validation
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$",$email))
		{
			echo "<script>","alert('Invalid Email');","</script>";
		}
		else
		{
			if($cert_reg != "")
			{
				if($clinic != "")
				{
					if($doctor != "")
					{
						if($town != "")
						{
							insert_registration($date,$cert,$cert1,$reg,$reg1,$clinic,$doctor,$address,$address2,$town,$postcode,$email,$telephone,$fax,$cert2,$reg2);
						}else
						{
							echo "<script>","alert('Filled in Town');","</script>";
						}
					}else
					{
						echo "<script>","alert('Filled in Doctor Name');","</script>";
					}
				}else
				{
					echo "<script>","alert('Filled in Clinic Name');","</script>";
				}
			}else
			{
				echo "<script>","alert('Please Choose Certificate No or Registration No');","</script>";
			}
		}
?>
<?php
	}
	else{}
?>

<body>
	
    <div class="wrapper">
    	
        <div class="side_bar" align="center">
        	<img src="images/logo_aga.png" style="padding:0px 0px 30px 0px;" />
            <ul>
                <li><a href="home.html">Home</a></li>
                <li><a href="understanding_hairloss.html">Understanding Hair Loss</a></li>
                <li><a href="stop_hairloss.html">How to Stop Hair Loss</a></li>
                <li><a href="male_hairloss.html">AGA (Male Hair Loss)</a></li>
                <li><a href="female_hairloss.html">Female Hair Loss</a></li>
                <li><a href="treatment_today.html">Treatment Today</a></li>
                <li><a href="faq.html">Frequently Asked Questions</a></li>
                <li><a href="news_updates.html">News Updates</a></li>
                <li><a href="find_doctor.php">Find a Doctor Near You</a></li>
            </ul>
            
        </div>
        
        <div class="content">
           	
            <div>
            	<div class="headline_bar"><h1 style="padding-left:15px;">Registration</h1></div>
           		
				<!-- Form Start -->
                <form method="post" name="registrationForm" onsubmit="return validateform(this)">
				<table style="width:700px;" border=0>
					<tr>
						<td valign="top" style="border-bottom:1px solid; padding-bottom:5px;">
						<table>
							<tr>
							<td><input type="radio" name="cert_reg" id="annual_practice" value="Annual Practicing Certificate No" onChange="disablefield();" checked="true" />&nbsp;&nbsp;Annual Practicing Certificate No.</td>
							<td><input type="text" name="cert1" id="id_cert1" size="10px;" value="<?php echo $cert1; ?>"/></td>
							<td>/</td>
							<td><input type="text" name="cert2" id="id_cert2" size="10px;" value="<?php echo $cert2; ?>"/></td>
							</tr>
							
							<tr>
							<td><input type="radio" name="cert_reg" id="mmc_registration" value=t"MMC Registration No" onChange="disablefield();" />&nbsp;&nbsp;MMC Registration No.</td>
							<td><input type="text" name="reg1" id="id_reg1" size="10px;" value="<?php echo $reg1; ?>"/></td>
							<td>/</td>
							<td><input type="text" name="reg2" id="id_reg2" size="10px;" value="<?php echo $reg2; ?>"/></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px;">
						<table border=0>
							<tr><td style="padding-left:20px;">Clinic Name</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_clinic" name="clinic" value="<?php echo $clinic; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Doctor's Name</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_doctor" name="doctor" value="<?php echo $doctor; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Address 1</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_address1" name="address" value="<?php echo $address; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Address 2</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_address2" name="address2" value="<?php echo $address2; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Town</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_town" name="town" value="<?php echo $town; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">PostCode</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_postcode" name="postcode" value="<?php echo $postcode; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Email</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_email" name="email" value="<?php echo $email; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Telephone</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_telephone" name="telephone" value="<?php echo $telephone; ?>"/></td></tr>
							<tr><td style="padding-left:20px;">Fax</td><td style="padding-left:45px;">:</td><td style="padding-left:60px;"><input type="text" id="id_fax" name="fax" value="<?php echo $fax; ?>"/></td></tr>
							<tr><td align="center" colspan="3" style="padding-top:5px;"><input type="button" value="Reset" name="reset" onclick="resetFunction()">&nbsp;<input type="submit" name="btn_submit" id="btn_submit" value="Submit"/></td></tr>
						</table>
						</td>
					</tr>
				</table>
				</form>
                <!-- Form End -->
            </div>
            
            <div class="footer">
            	
                <img src="images/image_7.jpg" /> 
            
            </div>
        </div>
        
	</div>
    
    
    
</body>

</html>
